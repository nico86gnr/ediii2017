/*
===============================================================================
 Name        : LedFrecVar.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Utilizando interrupciones, escriba y comente un codigo en C para LPC1769
 	 	 	   que emplee un un pulsador conectado al puerto P0.6 para modificar la frecuencia
 	 	 	   de parpadeo de un led conectado al puerto P0.8. Inicialmente el tiempo que
 	 	 	   el led se encuentra encendido, esta guardado en la posicion de memoria
 	 	 	   0x10002010 y el tiempo durante el cual se encuentra apagado, esta guardado
 	 	 	   en la posicion de memoria 0x10002014. Cuando se presiona por primera vez el
 	 	 	   pulsador estos tiempos se reducen a la mitad y cuando se presiona por segunda
 	 	 	   vez estos tiempos se reducen a un cuarto del valor inicial. Cuando se presio
 	 	 	   na por tercera vez el pulsador los tiempos de encendido y apagado del led
 	 	 	   vuelven a su valor original. Este ciclo se repite indefinidamente.
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

void InitVar(void);
void InitInput(void);
void InitOutput(void);

#define addrFIO0DIR 0x2009C000
#define addrFIO0SET 0x2009C018
#define addrFIO0CLR 0x2009C01C
#define addrIO0IntClr 0x4002808C
#define addrIO0IntEnF 0x40028094
#define addrISER0	  0xE000E100
#define addrENCENDIDO 0x10002010  //puntero a la direccion donde se encuentra el tiempo de encendido
#define addrAPAGADO 0x10002014	//puntero a la direccion donde se encuentra el tiempo de apagado

unsigned int volatile *const FIO0DIR=(unsigned int*)addrFIO0DIR;
unsigned int volatile *const FIO0SET=(unsigned int*)addrFIO0SET;
unsigned int volatile *const FIO0CLR=(unsigned int*)addrFIO0CLR;
unsigned int volatile *const ISER0=(unsigned int*)addrISER0;
unsigned int volatile *const IO0IntEnF=(unsigned int*)addrIO0IntEnF;
unsigned int volatile *const IO0IntClr=(unsigned int*)addrIO0IntClr;
unsigned int volatile *const ENCENDIDO=(unsigned int*)addrENCENDIDO;
unsigned int volatile *const APAGADO=(unsigned int*)addrAPAGADO;

int pulsar;		//contador para contar las veces que se presiono el pulsador
int encendido;
int apagado;
int i;

int main(void) {

	InitVar();		//funcion que inicializa las variables declaradas
	InitOutput();	//funcion que define al P0.22 como salida

	*IO0IntEnF |= (1 << 6);	//habilito interrupcion de GPIO0 del P0.6 por flanco desc.
	*ISER0 |= (1 << 21);	//habilito interrupcion externa EINT3

    while(1){

    	*FIO0SET |= (1 << 22);			//prendo el led
        for(i=0; i<encendido; i++){}	//espero un tiempo igal al valor que tengo en encendido
        *FIO0CLR |= (1 << 22);			//apago el led
        for(i=0; i<apagado; i++){}		//espero un tiempo igual al valor que tengo en apagado

    }
}

void InitVar(void){
	 pulsar = 0;
	 //*ENCENDIDO = 10000000;
	 //*APAGADO = 10000000;
	 encendido = *ENCENDIDO; //en encendido cargo el valor almacenado en la dir de memoria apuntado por *ENCENDIDO
	 apagado = *APAGADO;	//en apagado cargo el valor  almacenado en la dir de memoria apuntado por *APAGADO
}

void InitOutput(void){
	*FIO0DIR |= (1 << 22);
}

void EINT3_IRQHandler(void){

	pulsar++;
	for(i=4000000; i>0; i--){}	//retardo para anti-rebote
	if(pulsar < 3){ //si se presiono por primera o segunda vez
		encendido = (encendido >> 1); //divido por 2
	    apagado = (apagado >> 1); //divido por 2
	}
	else InitVar();		//si se presiono por tercera vez reinicio a los valores originales

	*IO0IntClr |= (1 << 6);	 //bajo bandera de interrupcion de GPI0 del P0.6
}
