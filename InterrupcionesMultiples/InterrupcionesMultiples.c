/*
===============================================================================
 Name        : InterrupcionesMultiples.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

void led2Init (void);

#define LED2		22

#define addrFIO0DIR 0x2009C000
#define addrFIO0PIN 0x2009C014
#define addrFIO0SET 0x2009C018
#define addrFIO0CLR 0x2009C01C

#define addrT0CTCR	0x40004070
#define addrT0TCR	0x40004004
#define addrT0MR0	0x40004018
#define addrT0MR1	0x4000401C
#define addrT0PR	0x4000400C
#define addrT0MCR	0x40004014
#define addrISER0	0xE000E100
#define addrT0IR	0x40004000

#define addrPINSEL4 	0x4002C010
#define addrPINMODE4	0x4002C050
#define addrEXTMODE		0x400FC148
#define addrEXTPOLAR	0x400FC14C
#define addrEXTINT		0x400FC140

#define addrIO0IntEnF 0x40028094
#define addrIO2IntEnF 0x400280B4
#define addrIOIntStatus 0x40028080
#define addrIO0IntClr 0x4002808C
#define addrIO2IntClr 0x400280AC

#define addrISER0	0xE000E100

#define FIO0DIR ((unsigned int volatile*) addrFIO0DIR)
#define FIO0PIN ((unsigned int volatile*) addrFIO0PIN)
#define FIO0SET ((unsigned int volatile*) addrFIO0SET)
#define FIO0CLR ((unsigned int volatile*) addrFIO0CLR)

#define T0CTCR ((unsigned int volatile*) addrT0CTCR)
#define T0TCR ((unsigned int volatile*) addrT0TCR)
#define T0MR0 ((unsigned int volatile*) addrT0MR0)
#define T0MR1 ((unsigned int volatile*) addrT0MR1)
#define T0PR ((unsigned int volatile*) addrT0PR)
#define T0MCR ((unsigned int volatile*) addrT0MCR)
#define T0IR ((unsigned int volatile*) addrT0IR)

#define PINSEL4 ((unsigned int volatile*) addrPINSEL4)
#define PINMODE4 ((unsigned int volatile*) addrPINMODE4)
#define EXTMODE ((unsigned int volatile*) addrEXTMODE)
#define EXTPOLAR ((unsigned int volatile*) addrEXTPOLAR)
#define EXTINT ((unsigned int volatile*) addrEXTINT)

#define IO0IntEnF ((unsigned int volatile*) addrIO0IntEnF)
#define IO2IntEnF ((unsigned int volatile*) addrIO2IntEnF)
#define IOIntStatus ((unsigned int volatile*) addrIOIntStatus)
#define IO0IntClr ((unsigned int volatile*) addrIO0IntClr)
#define IO2IntClr ((unsigned int volatile*) addrIO2IntClr)


#define ISER0 ((unsigned int volatile*) addrISER0)

int flag =0;

int main(void) {

	led2Init ();

    //CONFIGURACION DE INTERRUPCIONES

	 *T0CTCR &= ~3;   //configuro el timer0 como timer (no hace falta)
	 *T0PR=0;		//prescaler con cero  (no hace falta)

	 *T0MCR |= 1;			//habilito interrupcion por MR0=TC
	 *T0MCR |= (1 << 3);	//habilito interrupcion por MR1=TC
	 *T0MCR |= (1 << 4);	//reseteo el TC cuando MR1=TC


	 *PINSEL4 = (1 << 20);	//selecciono la funcion EINT0 para P2.10
	 // *PINMODE4 = 0;			activa pull-up
	 *EXTMODE |= 1;			//selecciono interrupcion por flanco
	 // *EXTPOLAR |= 0;		//por flanco descendente

     *IO0IntEnF |= (1 << 6);	//habilito interrupcion de GPIO0 del P0.6 por flanco desc.
     *IO2IntEnF |= (1 << 6);	//habilito interrupcion de GPIO0 del P2.6 por flanco desc.

     *ISER0 |= (1 << 18);		//habilito interrupciones externas
	 *ISER0 |= (1 << 21);	//habilito interrupcion externa EINT3
	 *ISER0 |= (1 << 1);	//habilito interrupcion externa EINT3

	 while(1){	//espero por una interrupcion



	 }

    return 0 ;
}

void led2Init (void){

	*FIO0DIR |= (1 << 22);
}

void TIMER0_IRQHandler(void){

	if((*T0IR & 1) == 1){     //testeo bandera MATH0

		*FIO0CLR |= (1 << LED2); //apago led
		*T0IR |= 2;				//bajo bandera
	}

	else if((*T0IR & 2) == 2 ){	 //testeo bandera MATH1

		*FIO0SET |= (1 << LED2);	//prendo led
		*T0IR |= 1;				 //bajo bandera
	}

}

void EINT0_IRQHandler(void){

	int k;
	for(k=4000000;k>0;k--){}

	*T0MR0=12500000;  //matchea a 0.5 seg
	*T0MR1=25000000;  //matchea a 1 seg

	if(*T0TCR == 0) *T0TCR |= 1;		//timer counter y prescaler habilitados para contar
	else {
		*T0TCR |= ( 1<<1 );
		*T0TCR &= ~( 1<<1 );
	}

	*EXTINT |= 1;				//bajo bandera de interrupcion externa

}

void EINT3_IRQHandler(void){

	int k;
	for(k=4000000; k>0; k--){}	//retardo para anti-rebote

	if( *IOIntStatus == 1){		//si la interrupcion es por puerto0

		if(*T0MR1 == 25000000 || *T0MR1 == 0 ){
			*T0MR1=25000;
			*T0MR0=2500;
		}

		else if(*T0MR0 < *T0MR1) *T0MR0 = *T0MR0 + 2500;

		if(*T0TCR == 0) *T0TCR |= 1;		//timer counter y prescaler habilitados para contar
		else {
			*T0TCR |= ( 1<<1 );
			*T0TCR &= ~( 1<<1 );
		}


		*IO0IntClr |= (1 << 6);
	}

	else if( *IOIntStatus == 4){  //si la interrupcion es por puerto2


		if(*T0MR1 == 25000000 || *T0MR1 == 0){
			*T0MR1=25000;
			*T0MR0=22500;
		}

		else if(*T0MR0 > 0) *T0MR0 = *T0MR0 - 2500;

		if(*T0TCR == 0) *T0TCR |= 1;		//timer counter y prescaler habilitados para contar
			else {
				*T0TCR |= ( 1<<1 );
				*T0TCR &= ~( 1<<1 );
			}

		*IO2IntClr |= (1 << 6);
	}
}
