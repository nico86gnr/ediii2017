/*
===============================================================================
 Name        : TareaTimer.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Implemente un código tal que se generen interrupciones en Match0 y Match1,
 	 	 	   en Match0 encendemos un led, en Match1 apagamos el led y reseteamos el
 	 	 	   contador del timer.
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

void led2Init (void);

#define addrFIO0DIR 0x2009C000
#define addrFIO0PIN 0x2009C014
#define addrFIO0SET 0x2009C018
#define addrFIO0CLR 0x2009C01C
#define addrT0CTCR	0x40004070
#define addrT0TCR	0x40004004
#define addrT0MR0	0x40004018
#define addrT0MR1	0x4000401C
#define addrT0PR	0x4000400C
#define addrT0MCR	0x40004014
#define addrISER0	0xE000E100
#define addrT0IR	0x40004000
#define LED2		22

#define FIO0DIR ((unsigned int volatile*) addrFIO0DIR)
#define FIO0PIN ((unsigned int volatile*) addrFIO0PIN)
#define FIO0SET ((unsigned int volatile*) addrFIO0SET)
#define FIO0CLR ((unsigned int volatile*) addrFIO0CLR)
#define T0CTCR ((unsigned int volatile*) addrT0CTCR)
#define T0TCR ((unsigned int volatile*) addrT0TCR)
#define T0MR0 ((unsigned int volatile*) addrT0MR0)
#define T0MR1 ((unsigned int volatile*) addrT0MR1)
#define T0PR ((unsigned int volatile*) addrT0PR)
#define T0MCR ((unsigned int volatile*) addrT0MCR)
#define ISER0 ((unsigned int volatile*) addrISER0)
#define T0IR ((unsigned int volatile*) addrT0IR)


int main(void) {

	led2Init();

    *T0CTCR &= ~3;   //configuro el timer0 como timer (no hace falta)
    *T0MR0=25000000;  //equivale a un segundo
    *T0MR1=50000000;  //equivale a dos segundos
    *T0PR=0;			//prescaler con cero  (no hace falta)

    *T0MCR |= 1;			//habilito interrupcion por MR0=TC
    *T0MCR |= (1 << 3);		//habilito interrupcion por MR1=TC
    *T0MCR |= (1 << 4);		//reseteo el TC cuando MR1=TC

    *ISER0 |= (1 << 1);		//habilito interrupciones por TIMER0

    *T0TCR |= 1;		//timer counter y prescaler habilitados para contar


    while(1) {  //espero por una interrupcion

    }

    return 0 ;
}

void led2Init (void){

	*FIO0DIR |= (1 << 22);
}

void TIMER0_IRQHandler(void){

	if((*T0IR & 1) == 1){     //testeo bandera MATH0

		*T0IR |= 1;				//bajo bandera
		*FIO0SET |= (1 << LED2);	//prendo led


	}

	else if((*T0IR & 2) == 2 ){		//testeo bandera MATH1

		*T0IR |= 2;				//bajo bandera
		*FIO0CLR |= (1 << LED2);	//apago led


	}
}

