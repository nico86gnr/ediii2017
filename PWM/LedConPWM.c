
/*
===============================================================================
 Name        : LedConPWM.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Generar, a través de bucles "for" una señal PWM, en un pin,
 tal que el ciclo de trabajo tenga los siguientes valores 0%, 25%, 50%, 75%,
 100% de modo que varíe la intensidad de iluminación de un led conectado
 a dicho pin. Por otra parte se tendrá un pulsador conectado a un pin,
 configurado como entrada. La señal de PWM (antes mencionada)
 cambiará de ciclo de trabajo de forma secuencial a medida que se vaya
 presionando el pulsador. La señal PWM puede comenzar en 0% de duty cicle
 (ciclo de trabajo), luego de presionarse el pulsador 1 vez el ciclo de
 trabajo pasará al 25%, luego de presionar una vez más pasará al 50%,
 luego a 75%, 100% y si se presiona una vez más pasará a 0% para repetir
 nuevamente la secuencia.
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"

#endif

#include <cr_section_macros.h>

// TODO: insert other include files here

// TODO: insert other definitions and declarations here
#define LED2 22
//DEFINI LAS DIRECCIONES
#define addr_FIO0DIR 0X2009C000
#define addr_FIO0CLR 0X2009C01C
#define addr_FIO0SET 0X2009C018
#define addr_FIO0PIN  0x2009C014

unsigned int volatile *const  FIO0DIR= (unsigned int*) addr_FIO0DIR;
unsigned int volatile *const  FIO0CLR= (unsigned int*) addr_FIO0CLR;
unsigned int volatile *const  FIO0SET= (unsigned int*) addr_FIO0SET;
unsigned int volatile *const  FIO0PIN= (unsigned int*) addr_FIO0PIN;

void led2Init (void);
void imputInit (void);

int main(void) {
	led2Init();
	    // Force the counter to be placed into memory
	    int x= 0 ;
	    int y=200000;
	    int a=0;// 0 cuando aumenta y 1 cuando baja
	    int i = 0 ;
	    int j=0;
	    int k;
    while(1) {
    	*FIO0SET= (1 << LED2); // prendo el led
    	for(j=x;j>0;j--)	//tiempo en alto
    	 	{
    	   	}
      	*FIO0CLR = (1 << LED2);
       	for(i=y;i>0;i--)	//tiempo en bajo
       	    {
        	}

       	if( (*FIO0PIN & (1 << 21)) == 0){ //si se presiono el pulsador
       		for(k=4000000;k>0;k--)	//retardo para anti-rebote
       		    	 	{
       		    	   	}
       		if( a == 0){
       			x=x+50000;	// aca se cambia el ciclo de trabajo
       			y=y-50000;
       		}
       		else {
       			x=0;	//aca se reinicia a un ciclo de trabajo del 0%
       			y=200000;
       			a=0;
       		}
       	}

       	if(y==0){a=1;}
    }
    return 0 ;
}

void led2Init (void) //configuro como salida el LED del pin 22
{
	*FIO0DIR |= (1 << LED2);
}

void imputInit (void) //configuro como entrada el P0.21 (PIN 23)
{					  //donde hay conectado un pulsador
	*FIO0DIR &= ~(1 << 21);
}
