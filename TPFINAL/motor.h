/*
 * motor.h
 *
 *  Created on: 14 de nov. de 2017
 *      Author: nico8
 */

#ifndef MOTOR_H_
#define MOTOR_H_

void stepper_motor(void);
void girarHaciaLDR1(void);
void girarHaciaLDR0(void);
void cambiarGiro(void);
void reiniciarMotor(void);

#endif /* MOTOR_H_ */
