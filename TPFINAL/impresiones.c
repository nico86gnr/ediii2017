/*
 * impresiones.c
 *
 *  Created on: 14 de nov. de 2017
 *      Author: nico8
 */

#include "vars.h"
#include "impresiones.h"
#include "stdio.h"

void imprimirParametros(){
	printf("\nPaso: %d ", paso);
	printf("\nPaso_max: %d ", paso_max);
	printf("\nlimite_sup: %d ", limite_sup);
	printf("\nlimite_inf: %d ", limite_inf);
}

void imprimirResultadosPasoAPaso(){ //muestro resultados de cada paso del motor
	printf("\nPaso: %d ", paso);
	//printf("\nvalor analogico: %f ",analogico);
	printf("\nresultado de conversion: %d ",resultadoADC0);
	printf("\nintensidad max:%d ", intensidad_max);
	printf("\n");
}

void imprimirResultadosFinales(){
	//imprimo los datos de la ubicacion donde se registro la intensidad maxima de luz
	printf("\nUbicacion ");
	printf("\nPasoMax: %d ", paso_max);
	printf("\nintensidad: %d ", intensidad_max);
	printf("\nAngulo: %d %s ", angulo,"grados");

}


