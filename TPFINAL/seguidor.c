/*
 * seguidor.c
 *
 *  Created on: 14 de nov. de 2017
 *      Author: nico8
 */

#include "motor.h"
#include "configuraciones.h"
#include "seguidor.h"
#include "impresiones.h"
#include "displays.h"
#include "vars.h"

#define umbral 75

void seguirLuz(){

	int k;
	sensarParaComparar();

	if( ((resultadoADC0-resultadoADC1) > umbral) && paso<limite_sup){

		girarHaciaLDR0();
		flagtimer=0;
		ida=1;
		timerConfig(); 			//habilito timer
		while(flagtimer==0); 	//espero interrupcion por timer pra que haga step=1
		flagtimer=0;
		timerConfig();			//habilito timer
		while(flagtimer==0);	//espero interrupcion por timer para que haga step=0
		//imprimirParametros();

	}
	else if( ((resultadoADC0-resultadoADC1) < -umbral) && paso>limite_inf){

		girarHaciaLDR1();
		flagtimer=0;
		vuelta=1;
		timerConfig(); 			//habilito timer
		while(flagtimer==0); 	//espero interrupcion por timer pra que haga step=1
		flagtimer=0;
		timerConfig();			//habilito timer
		while(flagtimer==0);	//espero interrupcion por timer para que haga step=0
		//imprimirParametros();
	}
	else {
		calcularAnguloSeguidor();
		printf("\nAngulo: %d %s ", angulo,"grados");
		mostrarDisplay();
		Enviar(centena+0x30);
		Enviar(decena+0x30);
		Enviar(unidad+0x30);
		Enviar(32);
		Enviar(103);
		Enviar(114);
		Enviar(97);
		Enviar(100);
		Enviar(111);
		Enviar(115);
		Enviar(10);
		for(k=500; k>0; k--){
			multiplexar();
		}
	}

}

void sensarParaComparar(){

	while( (*AD0DR0 & (1<<31)>>31) == 0){} //espero que termine la conversion
	resultadoADC0=(*AD0DR0>>4)&0xFFF;		//guardo resultado de la conversion del ldr
	while( (*AD0DR1 & (1<<31)>>31) == 0){} //espero que termine la conversion
	resultadoADC1=(*AD0DR1>>4)&0xFFF;		//guardo resultado de la conversion del ldr

	//printf("\nresultado de conversionADC0: %d ",resultadoADC0);
	//printf("\nresultado de conversionADC1: %d ",resultadoADC1);
	//printf("\npaso: %d \n",paso);
	//printf("\ndiferencia: %d \n",resultadoADC0-resultadoADC1);


}


