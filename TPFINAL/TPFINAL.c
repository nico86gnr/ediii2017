/*
===============================================================================
 Name        : TPFINAL.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>


#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include "stdio.h"
#include "configuraciones.h"
#include "motor.h"
#include "barrido.h"
#include "impresiones.h"
#include "seguidor.h"
#include "displays.h"
#include "vars.h"

int main(void) {
	int i;
	uint8_t vec[] = {80,111,115,105,99,105,111,110,32,100,101,32,109,97,121,111,114,32,105,110,116,101,110,115,105,100,97,100,58,10}; // de 0 a 9

	uart3Config();
	gpioConfig();
	adcConfig();
	MotorConfig();
	intGPIOConfig();	//habilito pulsador para regresar a la posicion inicial
	habilitarPulsador2();
	while(iniciarBarrido==0){}
	while(iniciarBarrido==1){
		deshabilitarPulsador2();
		iniciarBarrido=0;
		girarHaciaLDR0();
		barrido180();
		calcularAngulo();
		imprimirResultadosFinales();
		for(i=0;i<30;i++) Enviar(vec[i]); //se envia por la uart "angulos sensados:"
		mostrarDisplay();
		Enviar(centena+0x30);
		Enviar(decena+0x30);
		Enviar(unidad+0x30);
		Enviar(10);
		Enviar(83);
		Enviar(101);
		Enviar(103);
		Enviar(117);
		Enviar(105);
		Enviar(100);
		Enviar(111);
		Enviar(114);
		Enviar(58);
		Enviar(10);
		for(i=1000; i>0; i--){
			multiplexar();
		}
		habilitarPulsador1();
		while(flag_reiniciarMotor==0){
			seguirLuz();
		}
		deshabilitarPulsador1();
		habilitarPulsador2();
		while(iniciarBarrido==0){}
	}

    return 0 ;
}


void TIMER0_IRQHandler(void){

	deshabilitarTimer();
	if( (paso<limite_inf) && (flag_reiniciarMotor==0) && (barrido==1) ){

		barrido --;
		limite_inf=0;
		barrido=0;
		MotorConfig();

	}

	else if( (paso>limite_sup) && (flag_reiniciarMotor==0) && (barrido==2) ){

		paso--;
		barrido --;
		limite_inf=paso_max;
		cambiarGiro();
	}

	*T0IR = 1;
	flagtimer=1;
	reiniciarMotor();//antes de realizar el paso se fija si apretamos el motor
	stepper_motor();

}

void EINT3_IRQHandler(void){

	int k;
	for(k=4000000; k>0; k--){}	//retardo para anti-rebote
	//cambiarGiro();
	if( ( (*IO0IntStatF & (1<<26)) >>26) == 1){
		girarHaciaLDR1();
		timerConfig();          //reinicio timer
		*IO0IntClr |= (1 << 26);
		//deshabilitarPulsador();
		flag_reiniciarMotor=1; //quiero que reinicie valores
	}
	else if( ( (*IO0IntStatF & (1<<2) )>>2) == 1){
		iniciarBarrido=1;
		*IO0IntClr |= (1 << 2);
	}
}

