/*
 * displays.c
 *
 *  Created on: 16 de nov. de 2017
 *      Author: nico8
 */
#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>


#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include "vars.h"

void mostrarDisplay(){
	int salida_display[] = {164800, 384, 295616, 263104, 393600, 394048, 426752, 448, 426944, 393664}; // de 0 a 9
	int contador_angulo=0;
	//int ang =789;
	int i;

	while(contador_angulo<angulo){
		unidad++;
		while(unidad>9){

							unidad=unidad-10;
							decena++;
							while(decena>9){

												decena=decena-10;
												centena++;
						}

	}


		contador_angulo++;

	}
//tengo los valores de unidad decena y centena
	for(i = 0; i < 10; i++) {
				if(i == unidad) {
					/*
					 * si el valor de "input" es igual al de "i", se copia el elemento correspondiente de
					 * "output" hacia "fiopin" para ser mostrado en display
					 */
					display1 = salida_display[i];
				}
				if(i == decena) {

					display2 = salida_display[i];
				}
				if(i == centena) {

					display3 = salida_display[i];
								}

	}
	// ahora multiplexo los display


	}

void multiplexar(){

			int i;

			//*FIO0SET |= 426944;

			*FIO0SET |= (1 << 4);//se habilita el display1
			*FIO0MASK |= (1<<4)|(1<<5)|(1<<10);////enmascaro los bits de habilitacion de los displays

			*FIO0PIN =~display1;//muestro el valor en el display1

			for(i=0; i<10000; i++){}
			*FIO0MASK &=~(1 << 4);
			*FIO0MASK &=~(1 << 5);
			*FIO0MASK &=~(1 << 10);//desenmascaro los bits de habilitacion de los displays

			*FIO0CLR = (1 << 4);//deshabilito el display1
			*FIO0SET = (1 << 5);//habilito el display2

			*FIO0MASK |= (1<<4)|(1<<5)|(1<<10);//enmascaro los bits de habilitacion de los displays

			*FIO0PIN =~display2;//muestro el valor en el display1

			for(i=0; i<10000; i++){}
			*FIO0MASK &=~(1 << 4);
			*FIO0MASK &=~(1 << 5);
			*FIO0MASK &=~(1 << 10);
			*FIO0CLR |= (1 << 5);
			*FIO0SET |= (1 << 10);
			*FIO0MASK|= (1<<4)|(1<<5)|(1<<10);;

			*FIO0PIN =~display3;
			for(i=0; i<10000; i++){}
			*FIO0MASK &=~(1 << 4);
			*FIO0MASK &=~(1 << 5);
			*FIO0MASK &=~(1 << 10);
			*FIO0CLR |= (1 << 10);
			unidad=0;
			decena=0;
			centena=0;

}

void Enviar (uint8_t c){
	while((*U3LSR&(1<<5))==0){}
	*U3THR=c;
}


