/*
 * configuraciones.h
 *
 *  Created on: 4 de nov. de 2017
 *      Author: nico8
 */

#ifndef CONFIGURACIONES_H_
#define CONFIGURACIONES_H_

void gpioConfig(void);
void timerConfig(void);
void adcConfig(void);
void uart3Config(void);
void intGPIOConfig(void);
void startADC(void);
void deshabilitarTimer(void);
void MotorConfig(void);
void habilitarPulsador1();
void habilitarPulsador2();
void deshabilitarPulsador1();
void deshabilitarPulsador2();


#endif /* CONFIGURACIONES_H_ */
