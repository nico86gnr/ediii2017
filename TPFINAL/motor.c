/*
 * motor.c
 *
 *  Created on: 14 de nov. de 2017
 *      Author: nico8
 */

#include "configuraciones.h"
#include "vars.h"

void stepper_motor(){

	if ((*FIO2PIN & 1) == 0) {
		*FIO2SET |= (1<<0);		//step=1 hace un paso
		if(ida==1){
			paso=paso+1;
			ida=0;
		}
		else if(vuelta==1){
			paso=paso-1;
			vuelta=0;
		}
	}
	else *FIO2CLR |= (1<<0); //step=0

}

void cambiarGiro(){

	if( (*FIO2PIN & 2) == 0 ) *FIO2SET |= (1<<1);
	else *FIO2CLR |= (1<<1);

}

void girarHaciaLDR0(){

	*FIO2CLR |= (1<<1);
}

void girarHaciaLDR1(){

	*FIO2SET |= (1<<1);
}

void reiniciarMotor(){

	if(flag_reiniciarMotor==1){ //si aprete boton
		if(paso==0){ // si paso es igual a paso , volvio al origen y debo reiniciar todos los valores
			flag_reiniciarMotor=0;
			intensidad_max=10000;
			//*paso=0;
			paso_max=0;
			barrido=2; //reinicio las vueltas
			flagtimer=0;
			//*limite_sup=48;
			cambiarGiro();
		}
		else{
			vuelta=1;
			timerConfig();
		}
	}
}
