/*
 * configuraciones.c
 *
 *  Created on: 4 de nov. de 2017
 *      Author: nico8
 */


#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <stdio.h>
#define VAR_DECLS
#include "vars.h"
#include "configuraciones.h"

void gpioConfig(){

	*FIO2DIR |= (1<<0)|(1<<1); //salidas para el motor (step y dir)
	*FIO2DIR &= ~(1<<2); //entradas M0 M1 M2
	*FIO2DIR &= ~(1<<3);
	*FIO2DIR &= ~(1<<4);
	//*FIO0DIR |= (1<<0)|(1<<1)|(1<<18)|(1<<17)|(1<<15)|(1<<16)|(1<<23); //segmentos de los displays a-b-c-d-e-f-g
	//*FIO0DIR |= (1<<8)|(1<<7)|(1<<6); //pines de habilitacion de display
	//display
	*FIO0DIR |= (1<<9)|(1<<8)|(1<<7)|(1<<6)|(1<<18)|(1<<17)|(1<<15)|(1<<11); //pines 5,6,7,8,11,12,13 respectivamente segmentos de los displays a-b-c-d-e-f-g
	*FIO0MASK=0;
	*FIO0DIR |= (1<<4)|(1<<5)|(1<<10); //pines de habilitacion de display pines 38,39,40 en orden respectivo
	*FIO0CLR |= (1<<4)|(1<<5)|(1<<10);//todos los display apagados
}

void timerConfig(){

	*T0MCR |=(1<<0)|(1<<1); //interrumpe por MATCH0 y resetea TC cuando se produce el MATCH
	*T0MR0 = 300000;		//cargo MATCH0
	*T0CR |= (1<<1);		//reset del TC
	*T0CR =0;
	*T0CR |= 1 ;			//habilito timer
	*ISER0 |= (1<<1);		//habilito interrupcion por timer
}

void adcConfig(){

	*PINSEL1 |= (1<<14)|(1<<16); //habilito pines AD0.0(p0.23) ADC0.1(p0.24)
	*PINMODE1 |= (1<<15)|(1<<17); //deshabilita pull-up y pull-down
	*PCONP |= (1<<12);   //energiza modulo
	*AD0CR |= (1<<21);  //pone al adc en modo operacional
	*AD0CR |= (1<<0);  //habilito canal 0
	*AD0CR |= (1<<1);  //habilito canal 1
	*AD0CR |= (1<<8);  //divido por 2 (clkdiv +1) (clckADC=12,5MHZ)
	//*AD0CR |= (1<<9);  //divido por 2 (clkdiv +1) (clckADC=12,5MHZ)
	*AD0CR |= (1<<16);  //modo BURST
	//*AD0INTEN |= (1<<0); //|(1<<1); //habilito interrupcion en canal 0
	//*ISER0 |= (1<<22); //habilito interrupciones por ADC

}

void uart3Config(){

	/*ConfiguraciÃ³n de UART3*/
		LPC_SC->PCONP |= 1<<25; //1)-ENCENDEMOS EL PERIFERICO (UART3): *PCONP |= (1<<25);

		//*PCLKSEL1 &= ~(1<<18);	//2)- CLOCKING:PCLK_UART3 son bits 18 y 19 en 00 (por defecto)
		//*PCLKSEL1 &= ~(1<<19);

		*U3LCR |= (11<<0);		//3)- BAUD RATE
									//Fortmato de la trama (BITS 1:0): 8-bit character length (LPC_UART3->LCR |= 11;)
		*U3LCR |= (1<<7);			//habilitamos DLA para acceder a  DLL Y DLM (LPC_UART3->LCR |= 1<<7;)
		*U3DLL = 0xA1;				//LPC_UART3->DLL=0b10100001;
		*U3DLM = 0;					//LPC_UART3->DLM=0;
		*U3LCR &= 0b01111111;		//deshabilito el DLAB poniendolo en 0 (LPC_UART3->LCR &= 0b01111111;)
										//bit1:0:longitud de palabra: 8 bits
										//bit2: bits de stop
										//bit3: paridad habilitada
										//bit5:4: Tipo de paridad:0 (espacio)
										//bit6: con break control
										//bit7: deshabilito DLAB
								//4)- UART FIFO
								//5)-
		//*U3IER |= (1<<0);		//6)- INTERRUPCIONES:Enables the Receive Data Available interrupt for UARTn: LPC_UART3->IER=1;

		/* CONFIGURAMOS NVIC */

		//*ISER0 |= (1<<8); //NVIC_EnableIRQ(UART3_IRQn);
		/* CONFIGURAMOS TX Y RX */
		LPC_PINCON->PINSEL0|=0b1010; //P0.0 como TX y P0.1 como RX

}

void intGPIOConfig(){

	//*IO0IntEnF |= (1<<26);	//habilito interrupcion de GPIO0 del P0.26 por flanco desc.
	//*IO0IntEnF |= (1<<2);	//habilito interrupcion de GPIO0 del P0.26 por flanco desc.
	*ISER0 |= (1<<21); // habilito interrupcion externa 3
}

void MotorConfig(){ 	//defino la cantidad de pasos del motor (para un barrido
													//de 180 grados) y el angulo por
													//paso en funcion de los micropasos
	limite_inf=0;

	if( ( (*FIO2PIN & 28)>>2 )== 0){	//Full step
		limite_sup=24;
		anguloXpaso=7.5;
	}
	else if( ( (*FIO2PIN & 28)>>2 )== 1){ //1/2 step
		limite_sup=48;
		anguloXpaso=3.5;
	}
	else if( ( (*FIO2PIN & 28)>>2 )== 2){ //1/4 step
		limite_sup=96;
		anguloXpaso=1.875;
	}
	else if( ( (*FIO2PIN & 28)>>2 )== 3){ //1/8 step
		limite_sup=192;
		anguloXpaso=0.9375;
	}
	else if( ( (*FIO2PIN & 28)>>2 )== 4){ //1/16 step
		limite_sup=384;
		anguloXpaso=0.46875;
	}
	else if( ( (*FIO2PIN & 28)>>2 )>= 5){ //1/32 step
		limite_sup=768;
		anguloXpaso=0.234375;
	}
}


void startADC(){
	*AD0CR |= (1<<24);
}

void deshabilitarTimer(){
	*T0CR = 0;
}

void habilitarPulsador1(){
	*IO0IntEnF |= (1<<26);
}

void habilitarPulsador2(){
	*IO0IntEnF |= (1<<2);
}

void deshabilitarPulsador1(){
	*IO0IntEnF &= ~(1<<26);
}

void deshabilitarPulsador2(){
	*IO0IntEnF &= ~(1<<26);
}
