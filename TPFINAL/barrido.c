/*
 * barrido.c
 *
 *  Created on: 4 de nov. de 2017
 *      Author: nico8
 */


#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <stdio.h>
#include "vars.h"
#include "configuraciones.h"
#include "barrido.h"
#include "impresiones.h"


void sensarBarrido(){

	while( (*AD0DR0 & (1<<31)>>31) == 0){} //espero que termine la conversion
	resultadoADC0=(*AD0DR0>>4)&0xFFF;		//guardo resultado de la conversion del ldr

	if(resultadoADC0<intensidad_max) {		//si la conversion es la mas baja del barrido
		intensidad_max=resultadoADC0;		//guardo el valor de la conversion en la variable intensidad maxima
		paso_max=paso;					//guardo el paso donde se encuentra la intensidad maxima de luz
	}
}

void barrido180(){
	 while(barrido==2){ //va sensando en cada paso
	    flagtimer=0;
	    ida=1;
	    //startADC();
	    sensarBarrido();
	    //convertirAnalogico();
	    //imprimirResultadosPasoAPaso(paso,intensidad_max);
		timerConfig(); 			//habilito timer
		while(flagtimer==0); 	//espero interrupcion por timer pra que haga step=1
		flagtimer=0;
		timerConfig();			//habilito timer
		while(flagtimer==0);	//espero interrupcion por timer para que haga step=0
	}
	while(barrido==1){  	//al regresar no sensa, solo hace los pasos hasta posicionarse en el paso
		//imprimirParametros(paso,limite_sup,limite_inf);
		flagtimer=0;			//de mayor intensidad luminica (menor voltaje del ldr)
		vuelta=1;
		timerConfig();
		while(flagtimer==0);
		flagtimer=0;
		timerConfig();			//habilito timer
		while(flagtimer==0);
	}

}


void calcularAngulo(){
	angulo=paso_max*anguloXpaso;
}

void calcularAnguloSeguidor(){
	angulo=paso*anguloXpaso;
}

void convertirAnalogico(){   //convierte el resultado de la conversion digital a analogico
	analogico=(vref/4096)*resultadoADC0;
}

