/*
 * impresiones.h
 *
 *  Created on: 14 de nov. de 2017
 *      Author: nico8
 */

#ifndef IMPRESIONES_H_
#define IMPRESIONES_H_

void imprimirParametros(void);
void imprimirResultadosPasoAPaso(void);
void imprimirResultadosFinales(void);

#endif /* IMPRESIONES_H_ */
