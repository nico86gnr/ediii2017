/*
===============================================================================
 Name        : mi_proyecto.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here
void led2Init (void); // Set GPIO - P0_22 - to be output
// TODO: insert other definitions and declarations here
#define LED2 22
//DEFINI LAS DIRECCIONES
#define addr_FIO0DIR 0X2009C000
#define addr_FIO0CLR 0X2009C01C
#define addr_FIO0SET 0X2009C018

unsigned int volatile *const  FIO0DIR= (unsigned int*) addr_FIO0DIR;
unsigned int volatile *const  FIO0CLR= (unsigned int*) addr_FIO0CLR;
unsigned int volatile *const  FIO0SET= (unsigned int*) addr_FIO0SET;

int main(void) {

    // TODO: insert code here
led2Init();
    // Force the counter to be placed into memory
    int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {

    	for(i=10000000;i>0;i--)
    	{
    	}
    	*FIO0SET = (1 << LED2);	//se enciende el led
    	for(i=10000000;i>0;i--)//10000000
    	{
    	}
    	*FIO0CLR = (1 << LED2);	//se apaga el led
        i++ ;
    }
    return 0 ;
}

void led2Init (void) // Set GPIO - P0_22 - to be output
{
	*FIO0DIR |= (1 << LED2);
}
