/*
===============================================================================
 Name        : InterrExterna.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : generar una interrupcion con un pulsador por el pin con funcion
 	 	 	   EINT0 (P2.10 pin 51 en la placa) tal que se prenda y se apague un led
 	 	 	   cada vez que se presiona el pulsador
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

#define addrFIO0DIR 	0x2009C000
#define addrFIO0PIN 	0x2009C014
#define addrFIO0SET 	0x2009C018
#define addrFIO0CLR 	0x2009C01C
#define addrPINSEL4 	0x4002C010
#define addrPINMODE4	0x4002C050
#define addrEXTMODE		0x400FC148
#define addrEXTPOLAR	0x400FC14C
#define addrEXTINT		0x400FC140
#define addrISER0		0xE000E100
#define LED2			22

#define FIO0DIR ((unsigned int volatile*) addrFIO0DIR)
#define FIO0PIN ((unsigned int volatile*) addrFIO0PIN)
#define FIO0SET ((unsigned int volatile*) addrFIO0SET)
#define FIO0CLR ((unsigned int volatile*) addrFIO0CLR)
#define PINSEL4 ((unsigned int volatile*) addrPINSEL4)
#define PINMODE4 ((unsigned int volatile*) addrPINMODE4)
#define EXTMODE ((unsigned int volatile*) addrEXTMODE)
#define EXTPOLAR ((unsigned int volatile*) addrEXTPOLAR)
#define EXTINT ((unsigned int volatile*) addrEXTINT)
#define ISER0 ((unsigned int volatile*) addrISER0)

void led2Init (void);

int main(void) {

	led2Init();

   *PINSEL4 = (1 << 20);	//selecciono la funcion EINT0 para P2.10
  // *PINMODE4 = 0;			activa pull-up
   *EXTMODE |= 1;			//selecciono interrupcion por flanco
  // *EXTPOLAR |= 0;		//por flanco descendente
   *ISER0 |= (1 << 18);		//habilito interrupciones externas

   while(1){	//espero por una interrupcion

   }

   return 0;
}

void EINT0_IRQHandler(void){

	int k;
	for(k=4000000;k>0;k--){}
	if((*FIO0PIN & (1 << LED2)) == 0) *FIO0SET |= (1 << LED2);	//si esta apagado prendo led
	else *FIO0CLR |= (1 << LED2);	//si esta prendido lo apago

	*EXTINT |= 1;				//bajo bandera de interrupcion externa

}

void led2Init (void){

	*FIO0DIR |= (1 << 22);
}
