/*
===============================================================================
 Name        : deco7seg.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Ingresar un número binaro de 4 bits y mostrar este número en decimal a través de 2 display
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>


void displayInit(void); // habilitar display como salida
void inputInit(void); // habilitar pines de entrada
void Bin_BCD(void);

#define addrFIO0DIR 0x2009C000
#define addrFIO0PIN 0x2009C014
#define addrFIO0SET 0x2009C018
#define addrFIO0CLR 0x2009C01C
#define addrFIO0MASK 0x2009C010

#define FIO0DIR ((unsigned int volatile*) addrFIO0DIR)
#define FIO0PIN ((unsigned int volatile*) addrFIO0PIN)
#define FIO0SET ((unsigned int volatile*) addrFIO0SET)
#define FIO0CLR ((unsigned int volatile*) addrFIO0CLR)
#define FIO0MASK ((unsigned int volatile*) addrFIO0MASK)

uint32_t output[] = {491523, 32770, 8585219, 8486915, 8683522, 8749057, 8880129, 32771, 8880131, 8683523};
// "output" contiene los numeros que equivalen a los 10 numeros decimales convertidos al formato 7 segmentos

uint32_t unidades = 0; // variable donde se copiara el contenido de FIOPIN
uint32_t decenas = 0;

int main(void) {
	displayInit(); // habilitar display como salida
	inputInit(); // habilitar pines de entrada

	uint32_t i; // contador

/*	*FIO0PIN = output[0];
	*FIO0PIN = output[1];
	*FIO0PIN = output[2];
	*FIO0PIN = output[3];
	*FIO0PIN = output[4];
	*FIO0PIN = output[5];
	*FIO0PIN = output[6];
	*FIO0PIN = output[7];
	*FIO0PIN = output[8];
	*FIO0PIN = output[9];
*/
	while(1) {

		uint32_t display1 = 0; // variable donde se copiara el contenido de FIOPIN
		uint32_t display2 = 0;
		unidades = *FIO0PIN; // se copia el estado del Port0 a unidades
		unidades >>= 6; // se desplazan los bits 6 veces hacia la derecha para dejar LSB=p6, MSB=p9
		unidades &= 15; // se realiza AND bit a bit con el numero 15 (1111b) para limpiar los bits mayores al 3 (sino se guarda basura en el carry)
		decenas=0;
		Bin_BCD();

		for(i = 0; i < 10; i++) {
			if(i == unidades) {
				/*
				 * si el valor de "unidades" es igual al de "i", se copia su equivalente en 7 segmentos
				 * "output[i]" hacia "fiopin" para ser mostrado en display1
				 */
				display1 = output[i];
			}
			if(i == decenas) {     //se hace lo mismo con las decenas

				display2 = output[i];
			}
		}

		*FIO0SET = (1 << 25);		//se habilita el display1
		*FIO0MASK = (3 << 25);		//enmascaro los bits de habilitacion de los displays
		*FIO0PIN = display1;		//muestro el valor en el display1
		for(i=0; i<100000; i++){}	//retardo para multiplexacion
		*FIO0MASK = 0;				//desenmascaro los bits de habilitacion de los displays

		*FIO0CLR = (1 << 25);		//deshabilito el display1
		*FIO0SET = (1 << 26);		//habilito el display2

		*FIO0MASK = (3 << 25);		//enmascaro los bits de habilitacion de los displays
		*FIO0PIN = display2;		//muestro el valor en el display1
		for(i=0; i<100000; i++){}	//retardo para multiplexacion
		*FIO0MASK = 0;				//desenmascaro los bits de habilitacion de los displays
		*FIO0CLR = (1 << 26);		//deshabilito el display2


	}

    return 0 ;
}

void displayInit(void) {
	*FIO0DIR |= (1 << 0); // habilitar port0-pin9 como salida - segmento a
	*FIO0DIR |= (1 << 1); // habilitar port1-pin10 como salida - segmento b
	*FIO0DIR |= (1 << 15); // habilitar port15-pin13 como salida - segmento c
	*FIO0DIR |= (1 << 16); // habilitar port16-pin14 como salida - segmento d
	*FIO0DIR |= (1 << 17); // habilitar port17-pin12 como salida - segmento e
	*FIO0DIR |= (1 << 18); // habilitar port18-pin11 como salida - segmento f
	*FIO0DIR |= (1 << 23); // habilitar port23-pin15 como salida - segmento g
	*FIO0DIR |= (1 << 24); // habilitar port24-pin16 como salida - segmento dp
	*FIO0DIR |= (1 << 25); // habilitar port25-pin17 como salida - habilita disp1
	*FIO0DIR |= (1 << 26); // habilitar port26-pin18 como salida - habilita disp2
}

void inputInit(void) {
	*FIO0DIR &= ~(1 << 9); // habilitar pin 5 (GPIO 0.9) como entrada
	*FIO0DIR &= ~(1 << 8); // habilitar pin 6 (GPIO 0.8) como entrada
	*FIO0DIR &= ~(1 << 7); // habilitar pin 7 (GPIO 0.7) como entrada
	*FIO0DIR &= ~(1 << 6); // habilitar pin 8 (GPIO 0.6) como entrada
}

void Bin_BCD(void) {     //conversion de binario a BCD

	while(unidades>9){

				unidades=unidades-10;
				decenas++;
			}

}
