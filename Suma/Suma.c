/*
===============================================================================
 Name        : Suma.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description :  Ingresar 2 números de 4 bits cada uno, a través de 8 pines configurados como entradas,
 	 	 	 	y sacar por 5 pines el resultado de la suma de estos números
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here
void salidas (void);
void entradas (void);
// TODO: insert other definitions and declarations here

#define AddrFIO0DIR  0x2009C000
#define AddrFIO2DIR  0x2009C040
#define AddrFIO0PIN  0x2009C014
#define AddrFIO2PIN  0x2009C054

unsigned int volatile *const FIO0DIR=(unsigned int*)AddrFIO0DIR;
unsigned int volatile *const FIO2DIR=(unsigned int*)AddrFIO2DIR;
unsigned int volatile *const FIO0PIN=(unsigned int*)AddrFIO0PIN;
unsigned int volatile *const FIO2PIN=(unsigned int*)AddrFIO2PIN;


int main(void) {
    // TODO: insert code here

	salidas();
	entradas();

	while(1){
		unsigned int a= *FIO0PIN & 15;				     //cargo el estado de los pines P0.0-P0.3 en la variable a
		unsigned int b= (*FIO0PIN & (15 << 4)) >> 4;	//cargo el estado de los pines P0.4-P0.7 en la variable b
		unsigned int c= a+b;

		*FIO2PIN = c;			//cargo el resultado de la suma en los pines P2.0-P2.4
	}
    return 0 ;
}


void salidas (void) // se configuran los pines P2.0 a P2.4 como salidas
{
	*FIO2DIR |= 31;
}

void entradas (void) // se configuran los pines P0.0 a P0.7 como entradas
{
	*FIO0DIR &= ~255;
}

